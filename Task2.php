<?php
//Реализуйте генератор рандомных чисел, представленный классом Random. Интерфейс объекта включает в себя три функции:
//
//Конструктор. Принимает на вход seed, начальное число генератора псевдослучайных чисел
//getNext — метод, возвращающий новое случайное число
//reset — метод, сбрасывающий генератор на начальное значение

interface IRandom
{
    public function __construct(int $seed);

    public function getNext(): int;

    public function reset(): void;
}

class Random implements IRandom
{

// линейный конгруэнтный метод:
//где m — модуль (натуральное число, относительно которого вычисляет остаток от деления, m >= 2);
//a — множитель (0 <= a < m);
//c — приращение (0 <= c < m),
// seed — начальное значение (0 <= seed < m).

    const a = 15;
    const c = 45;
    const m = 130;
    private int $next;

    public function __construct(private int $seed)
    {
        $this->next = $this->seed;
    }

    public function getNext(): int
    {
        $this->next = $this->next * self::a + self::c;

        return $this->next % self::m;
    }

    public function reset(): void
    {
        $this->next = $this->seed;
    }
}

$seq = new Random(100);
$result1 = $seq->getNext();
$result2 = $seq->getNext();

echo $result1 . '<br>';
echo $result2 . '<br>';

echo ($result1 != $result2 ? "ok" : "no ok") . '<br>'; // true

$seq->reset();

$result21 = $seq->getNext();
$result22 = $seq->getNext();

echo ($result1 === $result21 ? "ok" : "no ok") . '<br>'; // true
echo ($result2 === $result22 ? "ok" : "no ok") . '<br>'; // true