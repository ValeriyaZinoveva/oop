<?php
// Реализуйте класс Circle для описания кругов. У круга есть только одно свойство - его радиус.
// Реализуйте методы getArea и getCircumference, которые возвращают площадь и периметр круга соответственно.

class Circle
{

//    public function __construct(private float $radius)
//    {
//    }

private float $radius;

public function __construct (float $radius)
{
    $this->radius = $radius;
}
    public function getArea(): float
    {
        return pi() * $this->radius * $this->radius;
    }

    public function getCircumrefence(): float
    {
        return 2 * pi() * $this->radius;
    }
}

$firstCircle = new Circle(10);

echo "Area = " . $firstCircle->getArea() . "<br>";
echo "Circumrefence = " . $firstCircle->getCircumrefence() . "<br>";