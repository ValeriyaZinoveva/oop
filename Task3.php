<?php

interface IUrl
{
    public function getScheme(): string;

    public function getHost(): string;

    public function getQueryParams(): array;

    public function getQueryParam(string $key, string $defaultValue = ''): string;

}

class Url implements IUrl
{
    private string $scheme;
    private string $host;
    private array $queryParams = [];

    public function __construct(string $url)
    {
        $parseUrlArray = parse_url($url);
        $this->scheme = $parseUrlArray["scheme"];
        $this->host = $parseUrlArray["host"];
        parse_str($parseUrlArray["query"], $this->queryParams);
    }

    public function getScheme(): string
    {
        return $this->scheme;
    }

    public function getHost(): string
    {
        return $this->host;
    }

    public function getQueryParams(): array
    {
        return $this->queryParams;
    }

    public function getQueryParam(string $key, string $defaultValue = ''): string
    {
        return $this->queryParams[$key] ?? $defaultValue;
    }
}

$url = new Url('http://yandex.ru?key=value&key2=value2');

echo ($url->getScheme()) . '<br>'; // http
echo ($url->getHost()) . '<br>'; // yandex.ru
var_dump($url->getQueryParams());
// [
//     'key' => 'value',
//     'key2' => 'value2'
// ];
echo $url->getQueryParam('key'); // value
// второй параметр - значение по умолчанию
echo $url->getQueryParam('key2', 'lala'); // value2
echo $url->getQueryParam('new', 'ehu'); // ehu